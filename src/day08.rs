pub struct DisplayOut<'a> {
    possibilities: Vec<&'a str>,
    outputs: Vec<&'a str>,
}

pub fn generator(input: &str) -> Vec<DisplayOut> {
    let mut result: Vec<DisplayOut> = vec![];

    for line in input.split('\n') {
        let line_split = line.split(" | ").collect::<Vec<_>>();

        result.push(DisplayOut { possibilities: line_split[0].split(' ').collect::<Vec<_>>(), outputs: line_split[1].split(' ').collect::<Vec<_>>() });
    }

    result
}

pub fn part1(input: &[DisplayOut]) -> u64 {
    // identify 1 4 7 and 8 in the output
    let mut counter = 0;

    for item in input {
        for output in &item.outputs {
            match output.len() {
                2 => {
                    // its a 1
                    counter += 1;
                }

                4 => {
                    // its a 4
                    counter += 1;
                }

                3 => {
                    // its a 7
                    counter += 1;
                }
                7 => {
                    // its a 8
                    counter += 1;
                }
                _ => {}
            }
        }
    }

    counter
}

pub fn part2(input: &[DisplayOut]) -> u64 {
    // identify 1 4 7 and 8 in the output
    let mut counter = 0;

    for item in input {
        let mut aaa: Option<char> = None;
        let mut bbb: Option<char> = None;
        let mut ccc: Option<char> = None;
        let ddd: Option<char>;
        let mut eee: Option<char> = None;
        let ggg: Option<char>;

        let mut one: Vec<char> = vec![];
        let three: Vec<char>;
        let mut four: Vec<char> = vec![];
        let mut six: Vec<char> = vec![];
        let mut seven: Vec<char> = vec![];
        let mut eight: Vec<char> = vec![];
        let nine: Vec<char>;

        // 2 3 5
        let mut fives: Vec<Vec<char>> = vec![];
        // 0 6 9
        let mut sixes: Vec<Vec<char>> = vec![];

        for numbers in &item.possibilities {
            let vec_of_chars = numbers.chars().collect::<Vec<_>>();

            match numbers.len() {
                // its a 1
                2 => one = vec_of_chars,
                // its a 7
                3 => seven = vec_of_chars,
                // its a 4
                4 => four = vec_of_chars,

                5 => fives.push(vec_of_chars),
                6 => sixes.push(vec_of_chars),

                // its a 8
                7 => eight = vec_of_chars,
                _ => {}
            }
        }

        // compare 1 and 7 to get aaa
        for character in &seven {
            // the character that is not in one is aaa
            if !one.iter().any(|x| x == character) {
                aaa = Some(*character);
            }
        }

        // compare 4, 9 (in sixes) and aaa to get ggg
        match tester_generalised(&sixes, &four, &[aaa.unwrap()]) {
            None => continue,
            Some(x) => {
                ggg = Some(x.requested_char);
                nine = x.requested_number;
                sixes = x.depleted_vec;
            }
        }

        // compare 1, 3 (in fives) and aaa/ggg to get ddd
        match tester_generalised(&fives, &one, &[aaa.unwrap(), ggg.unwrap()]) {
            None => continue,
            Some(x) => {
                ddd = Some(x.requested_char);
                three = x.requested_number;
            }
        }

        // aaa
        // ddd
        // ggg

        // 8 and 9 to get eee
        for character in &eight {
            if !nine.iter().any(|x| x == character) {
                eee = Some(*character);
            }
        }

        // aaa
        // ddd
        // eee
        // ggg

        // compare 9 and 3 to get b
        for character in &nine {
            if !three.iter().any(|x| x == character) {
                bbb = Some(*character);
            }
        }

        // aaa
        // bbb
        // ddd
        // eee
        // ggg

        // loop through sixes (contains 0 and 6)
        // if its missing ddd it is 6
        for (i, item) in sixes.clone().iter().enumerate() {
            match item.iter().position(|&x| x == ddd.unwrap()) {
                None => {
                    continue;
                }
                Some(_) => {
                    sixes.remove(i);
                    six = item.to_owned();
                }
            }
        }

        // compare 6 and 8 to get c
        for character in eight {
            if !six.iter().any(|&x| x == character) {
                ccc = Some(character);
            }
        }

        // aaa
        // bbb
        // ccc
        // ddd
        // eee
        // ggg

        // compare 1 and c to get f
        let mut tmp = one.clone();
        match tmp.iter().position(|&x| x == ccc.unwrap()) {
            None => {}
            Some(x) => {
                tmp.remove(x);
            }
        }

        // got all teh mappings

        let mut number_vec: Vec<char> = vec![];

        for output in item.outputs.clone() {
            let vec_of_chars = output.chars().collect::<Vec<_>>();
            match vec_of_chars.len() {
                // its a 1
                2 => {
                    number_vec.push('1');
                }
                // its a 7
                3 => {
                    number_vec.push('7');
                }
                // its a 4
                4 => {
                    number_vec.push('4');
                }

                5 => {
                    // 2 3 5
                    // 2 a c d e g
                    // 3 a c d f g
                    // 5 a b d f g

                    match vec_of_chars.iter().position(|&x| x == bbb.unwrap()) {
                        Some(_) => {
                            number_vec.push('5');
                        }
                        None => match vec_of_chars.iter().position(|&x| x == eee.unwrap()) {
                            Some(_) => {
                                number_vec.push('2');
                            }
                            None => {
                                number_vec.push('3');
                            }
                        },
                    }
                }
                6 => {
                    // 0 6 9

                    // 0 a b c   e f g
                    // 6 a b   d e f g
                    // 9 a b c d   f g

                    match vec_of_chars.iter().position(|&x| x == ddd.unwrap()) {
                        Some(_) => match vec_of_chars.iter().position(|&x| x == eee.unwrap()) {
                            Some(_) => {
                                number_vec.push('6');
                            }
                            None => {
                                number_vec.push('9');
                            }
                        },
                        None => {
                            number_vec.push('0');
                        }
                    }
                }

                // its a 8
                7 => {
                    number_vec.push('8');
                }
                _ => {}
            }
        }

        let number_string = number_vec.iter().collect::<String>();

        //println!("{}", &number_string);
        if let Ok(x) = number_string.parse::<u64>() {
            counter += x;
        }
    }

    counter
}

struct TesterResponse {
    requested_char: char,
    requested_number: Vec<char>,
    depleted_vec: Vec<Vec<char>>,
}

fn tester_generalised(test_vec: &[Vec<char>], known_vec: &[char], known_chars: &[char]) -> Option<TesterResponse> {
    let mut for_depleted = test_vec.to_owned();

    'outer: for (i, testing) in test_vec.iter().enumerate() {
        let mut test_array = testing.clone();

        for character in known_vec {
            // the character that is not in one is aaa
            match test_array.iter().position(|x| x == character) {
                None => {
                    continue 'outer;
                }
                Some(position) => {
                    test_array.remove(position);
                }
            }
        }

        // filter out known characters
        for character in known_chars {
            match test_array.iter().position(|x| x == character) {
                None => {
                    continue 'outer;
                }
                Some(position) => {
                    test_array.remove(position);
                }
            }
        }

        if test_array.len() == 1 {
            for_depleted.remove(i);

            return Some(TesterResponse { requested_char: test_array[0], requested_number: testing.to_owned(), depleted_vec: for_depleted });
        }
    }

    None
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";

    #[test]
    fn part1_test() {
        assert_eq!(part1(&generator(EXAMPLE)), 26);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&generator(EXAMPLE)), 61229);
    }
}
