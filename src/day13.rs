#[derive(Debug)]
pub struct Generator {
    // turn teh seed into an array of pairs
    coordinates: Vec<(usize, usize)>,
    max: (usize, usize),
    instructions: Vec<Command>,
}

// order, line
#[derive(Debug)]
enum Command {
    X(usize),
    Y(usize),
}

pub fn generator(input: &str) -> Generator {
    let mut input_split = input.split('\n');

    let mut generator = Generator { coordinates: vec![], max: (0, 0), instructions: vec![] };

    for x_str in input_split.by_ref() {
        if x_str.is_empty() {
            break;
        }

        let tmp = x_str.split(',').collect::<Vec<_>>();
        let x = tmp[1].parse::<usize>().unwrap();
        let y = tmp[0].parse::<usize>().unwrap();

        generator.coordinates.push((x, y));
    }

    for x in input_split {
        // fold along y=7
        let tmp = x.replace("fold along ", "");
        let command = tmp.split('=').collect::<Vec<_>>();

        let line = command[1].parse::<usize>().unwrap();

        match command[0] {
            "x" => {
                generator.instructions.push(Command::X(line));
                if line > generator.max.1 {
                    generator.max.1 = line;
                }
            }
            "y" => {
                generator.instructions.push(Command::Y(line));
                if line > generator.max.0 {
                    generator.max.0 = line;
                }
            }
            &_ => {}
        }
    }

    generator
}

pub fn part1_v1(input: &Generator) -> u64 {
    general(input, 1, false)
}

fn general(input: &Generator, fold_limit: usize, output: bool) -> u64 {
    let mut sheet = create_sheet(input);

    for (folds, command) in input.instructions.iter().enumerate() {
        if folds >= fold_limit {
            break;
        }
        match command {
            Command::Y(position) => {
                // fold up

                // keep the top, split off the bottom

                let mut tmp = sheet.split_off(*position);
                tmp.remove(0);
                tmp.reverse();

                // loop through
                for (i, row) in tmp.iter().enumerate() {
                    for (j, val) in row.iter().enumerate() {
                        sheet[i][j] += *val;
                    }
                }
            }
            Command::X(position) => {
                let mut tmp = vec![];
                for mut first in sheet {
                    let mut second = first.split_off(*position);
                    // remove the dividing line
                    second.remove(0);
                    second.reverse();

                    for (i, val) in second.iter().enumerate() {
                        first[i] += *val;
                    }

                    tmp.push(first);
                }

                sheet = tmp;
            }
        }
    }

    let mut count = 0;
    for line in &sheet {
        for val in line {
            if val > &0 {
                count += 1;

                if output {
                    print!("█");
                }
            } else if output {
                print!(".");
            }
        }
        if output {
            println!();
        }
    }

    count
}

pub fn part2_v1(input: &Generator) -> u64 {
    general(input, input.instructions.len(), false)
}

fn create_sheet(input: &Generator) -> Vec<Vec<u32>> {
    // x down, y across
    let mut sheet = vec![vec![0; (input.max.1 * 2) + 1]; (input.max.0 * 2) + 1];
    for line in &input.coordinates {
        let (i, j) = line;
        sheet[*i][*j] += 1;
    }

    sheet
}

pub fn part1_v2(input: &Generator) -> u64 {
    general_v2(input, 1, false)
}

pub fn part2_v2(input: &Generator) -> u64 {
    general_v2(input, input.instructions.len(), false)
}

pub fn part2_v2_print(input: &Generator) -> u64 {
    general_v2(input, input.instructions.len(), true)
}

// keep it as an array of coordinates
fn general_v2(input: &Generator, fold_limit: usize, output: bool) -> u64 {
    let mut result_vec = input.coordinates.clone();

    for (folds, command) in input.instructions.iter().enumerate() {
        if folds >= fold_limit {
            break;
        }

        match command {
            Command::Y(line) => {
                result_vec = result_vec
                    .into_iter()
                    .map(|coordinate| {
                        if &coordinate.0 < line {
                            return coordinate;
                        }

                        // new_x = coordinate.x - (coordinate.x - line) - (coordinate.x - line)
                        ((line + line) - coordinate.0, coordinate.1)
                    })
                    .collect::<Vec<_>>();
            }
            Command::X(line) => {
                result_vec = result_vec
                    .into_iter()
                    .map(|coordinate| {
                        if &coordinate.1 < line {
                            return coordinate;
                        }

                        // new_x = coordinate.x - (coordinate.x - line) - (coordinate.x - line)
                        (coordinate.0, (line + line) - coordinate.1)
                    })
                    .collect::<Vec<_>>();
            }
        }
    }
    result_vec.sort_unstable();
    result_vec.dedup();

    if output {
        // row, col
        let mut max = (0, 0);
        for line in &result_vec {
            if line.0 > max.0 {
                max.0 = line.0
            }
            if line.1 > max.1 {
                max.1 = line.1
            }
        }

        let mut sheet = vec![vec![0; (max.1) + 1]; (max.0) + 1];
        for line in &result_vec {
            let (i, j) = line;
            sheet[*i][*j] += 1;
        }
        for line in &sheet {
            for val in line {
                if val > &0 {
                    print!("█");
                } else {
                    print!(" ");
                }
            }
            println!();
        }
    }

    result_vec.len() as u64
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5";

    #[test]
    fn part1_test() {
        assert_eq!(part1_v1(&generator(EXAMPLE)), 17);
    }

    #[test]
    fn part1_test_v2() {
        assert_eq!(part1_v2(&generator(EXAMPLE)), 17);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2_v1(&generator(EXAMPLE)), 2188189693529);
    }
}
