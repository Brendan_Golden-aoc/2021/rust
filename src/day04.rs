pub struct BoardData {
    numbers: Vec<i32>,
    boards: Vec<Vec<Vec<i32>>>,
}

pub fn generator(input: &str) -> BoardData {
    let mut result = BoardData { numbers: vec![], boards: vec![] };

    let mut board: Vec<Vec<i32>> = vec![];
    for line in input.split('\n') {
        if result.numbers.is_empty() {
            for number in line.split(',') {
                if let Ok(x) = number.parse::<i32>() {
                    result.numbers.push(x)
                }
            }
            // skip[ top the next line
            continue;
        }

        if line.is_empty() {
            if !board.is_empty() {
                result.boards.push(board);
                board = vec![];
            }
            continue;
        }

        let mut row: Vec<i32> = vec![];
        for number in line.split(' ') {
            if let Ok(x) = number.parse::<i32>() {
                row.push(x)
            }
        }
        board.push(row)
    }
    if !board.is_empty() {
        result.boards.push(board);
    }

    result
}

pub fn part1(input: &BoardData) -> i32 {
    let mut winner = (usize::MAX, 0);

    for board in &input.boards {
        match process_board(&input.numbers, board) {
            None => {}
            Some((rounds, score)) => {
                if rounds < winner.0 {
                    winner.0 = rounds;
                    winner.1 = score;
                }
            }
        }
    }
    winner.1
}

pub fn part2(input: &BoardData) -> i32 {
    let mut winner = (0, 0);

    for board in &input.boards {
        match process_board(&input.numbers, board) {
            None => {}
            Some((rounds, score)) => {
                if rounds > winner.0 {
                    winner.0 = rounds;
                    winner.1 = score;
                }
            }
        }
    }
    winner.1
}

fn process_board(numbers: &[i32], board: &[Vec<i32>]) -> Option<(usize, i32)> {
    let mut board_processing = vec![vec![0; board[0].len()]; board.len()];
    let mut numbers_used = vec![];

    for (i, number) in numbers.iter().enumerate() {
        // search board for the number, if found mark it on board_processing
        for (row_number, row) in board.iter().enumerate() {
            for (col_number, col) in row.iter().enumerate() {
                if col == number {
                    board_processing[row_number][col_number] = 1;
                    numbers_used.push(number);
                }
            }
        }

        //if i < 5 {continue}
        // fit analysing the row/col here

        let mut winner = false;

        let mut col_checker = vec![0; board.len()];
        for row in board_processing.clone() {
            let mut row_hits = 0;
            for (col_number, col) in row.iter().enumerate() {
                row_hits += col;
                col_checker[col_number] += col;
            }

            if row_hits == row.len() {
                winner = true;
                break;
            }
        }

        // loop[ through the col checker
        for col in col_checker {
            if col == board.len() {
                winner = true;
                break;
            }
        }

        if winner {
            // return i and (sum of all unmakrked numbers) * number

            // loop through again to get unbmarked numbers
            let mut unmarked_total = 0;

            for (row_number, row) in board_processing.iter().enumerate() {
                for (col_number, col) in row.iter().enumerate() {
                    if col != &1 {
                        unmarked_total += board[row_number][col_number];
                    }
                }
            }

            return Some((i, unmarked_total * number));
        }
    }

    None
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7";

    #[test]
    fn part1_test() {
        assert_eq!(part1(&generator(EXAMPLE)), 4512);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&generator(EXAMPLE)), 1924);
    }
}
