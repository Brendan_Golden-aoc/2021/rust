use std::collections::HashMap;

#[derive(Debug)]
pub struct Generator<'a> {
    // turn teh seed into an array of pairs
    seed: Vec<String>,
    seed_raw: Vec<char>,
    // transitions
    transitions: Vec<Transition<'a>>,
}
#[derive(Debug)]
pub struct Transition<'a> {
    from: &'a str,
    character: char,
    to: [String; 2],
}

// each pair will create twop more pairs
// NN -> C
// NC and CN

pub fn generator(input: &str) -> Generator {
    let mut input_split = input.split('\n');

    let mut generator = Generator { seed: vec![], seed_raw: vec![], transitions: vec![] };

    if let Some(x) = input_split.next() {
        generator.seed_raw = x.chars().collect::<Vec<_>>();
        let mut position = 1;
        while position < generator.seed_raw.len() {
            generator.seed.push(String::from_iter([generator.seed_raw[position - 1], generator.seed_raw[position]]));
            position += 1;
        }
    }

    // skip over empty line
    input_split.next();

    //let mut transitions = vec![];
    for line in input_split {
        let tmp = line.split(" -> ").collect::<Vec<_>>();

        let from = tmp[0];

        let to_tmp = tmp[1];

        let from_split = from.split("").collect::<Vec<_>>();

        //println!("from_split {:?}", from_split);
        generator.transitions.push(Transition { from, character: tmp[1].chars().collect::<Vec<char>>()[0], to: [String::from_iter([from_split[1], to_tmp]), String::from_iter([to_tmp, from_split[2]])] })
    }

    generator
}

pub fn part1(input: &Generator) -> u64 {
    general(input, 10)
}

fn general(input: &Generator, iterations: u64) -> u64 {
    let mut pairs: HashMap<&str, u64> = HashMap::new();
    let mut characters: HashMap<char, u64> = HashMap::new();

    // create initial pairs
    for pair in &input.seed {
        match pairs.get_mut(pair.as_str()) {
            None => {
                pairs.insert(pair, 1);
            }
            Some(entry) => {
                *entry += 1;
            }
        }
    }

    // get teh characters of initial pairs
    for character in &input.seed_raw {
        match characters.get_mut(character) {
            None => {
                characters.insert(*character, 1);
            }
            Some(entry) => {
                *entry += 1;
            }
        }
    }

    let mut i = 0;
    while i < iterations {
        let mut pairs_next: HashMap<&str, u64> = HashMap::new();

        for (key, val) in pairs.iter() {
            // loop through teh transitions?
            for transition in &input.transitions {
                if &transition.from != key {
                    continue;
                }

                match characters.get_mut(&transition.character) {
                    None => {
                        characters.insert(transition.character, *val);
                    }
                    Some(entry) => {
                        *entry += *val;
                    }
                }

                for new_pair in &transition.to {
                    match pairs_next.get_mut(&new_pair.as_str()) {
                        None => {
                            pairs_next.insert(new_pair, *val);
                        }
                        Some(entry) => {
                            *entry += *val;
                        }
                    }
                }
            }
        }

        // set it up for the next loop
        pairs = pairs_next;

        i += 1;
    }

    let mut min_max = (u64::MAX, u64::MIN);

    for val in characters.values() {
        if val < &min_max.0 {
            min_max.0 = *val;
        }
        if val > &min_max.1 {
            min_max.1 = *val;
        }
    }
    min_max.1 - min_max.0
}

pub fn part2(input: &Generator) -> u64 {
    general(input, 40)
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C";

    #[test]
    fn part1_test() {
        assert_eq!(part1(&generator(EXAMPLE)), 1588);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&generator(EXAMPLE)), 2188189693529);
    }
}
