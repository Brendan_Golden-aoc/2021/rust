pub fn generator(input: &str) -> Vec<u64> {
    let mut crabs: Vec<u64> = vec![];

    for line in input.split('\n') {
        for number in line.split(',') {
            if let Ok(x) = number.parse::<u64>() {
                crabs.push(x)
            }
        }
    }

    crabs
}

pub fn part1(input: &[u64]) -> u64 {
    let mut max = 0;
    let mut min = u64::MAX;

    for crab in input {
        if crab > &max {
            max = *crab;
        }
        if crab < &min {
            min = *crab;
        }
    }

    let mut min_fuel = u64::MAX;
    for i in min..=max {
        let mut fuel = 0;
        for crab in input {
            if crab > &i {
                fuel += *crab - i
            }
            if crab < &i {
                fuel += i - *crab
            }
        }
        if fuel < min_fuel {
            min_fuel = fuel
        }
    }

    min_fuel
}

pub fn part2(input: &[u64]) -> u64 {
    let mut max = 0;
    let mut min = u64::MAX;

    for crab in input {
        if crab > &max {
            max = *crab;
        }
        if crab < &min {
            min = *crab;
        }
    }

    let mut min_fuel = u64::MAX;
    for i in min..=max {
        let mut fuel = 0;
        for crab in input {
            let mut distance = 0;
            if crab > &i {
                distance += *crab - i
            }
            if crab < &i {
                distance += i - *crab
            }
            fuel += (distance * (distance + 1)) / 2;
        }

        if fuel < min_fuel {
            min_fuel = fuel
        }
    }

    min_fuel
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "16,1,2,0,4,2,7,1,2,14";

    #[test]
    fn part1_test() {
        assert_eq!(part1(&generator(EXAMPLE)), 37);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&generator(EXAMPLE)), 168);
    }
}
