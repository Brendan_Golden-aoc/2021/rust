pub struct Route<'a> {
    start: &'a str,
    end: &'a str,
}

pub fn generator(input: &str) -> Vec<Route> {
    let mut result: Vec<Route> = vec![];

    for line in input.split('\n') {
        let line_split = line.split(" | ").collect::<Vec<_>>();

        result.push(Route {
            start: line_split[0],
            end: line_split[1]
        });
    }

    result
}

pub fn part1(input: &[Route]) -> u64 {
    0
}

pub fn part2(input: &[Route]) -> u64 {
    0
}


#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE1: &'static str = "start-A
start-b
A-c
A-b
b-d
A-end
b-end";

    #[test]
    fn part1_test() {
        assert_eq!(part1(&generator(EXAMPLE1)), 10);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&generator(EXAMPLE1)), 61229);
    }
}
