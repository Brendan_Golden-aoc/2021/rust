use std::collections::HashMap;


pub fn generator(input: &str) -> (HashMap<String, HashMap<String, HashMap<String, char>>>, Vec<String>) {
    let (algo_raw, image_raw) = input.split_once("\n\n").unwrap();

    let mut index = 0;
    let mut top_hash: HashMap<String, HashMap<String, HashMap<String, char>>> = HashMap::new();
    let mut top = String::from("...");
    let mut top_num = 0;
    // top
    for _ in 0..=7 {
        let mut middle_hash: HashMap<String, HashMap<String, char>> = HashMap::new();
        let mut middle = String::from("...");
        let mut middle_num = 0;

        // middle
        for _ in 0..=7 {

            let mut bottom_hash: HashMap<String, char> = HashMap::new();
            let mut bottom = String::from("...");
            let mut bottom_num = 0;

            for _ in 0..=7 {
                let char = algo_raw.chars().nth(index).unwrap();

                bottom_hash.insert(bottom.clone(), char);

                // update teh index
                index += 1;

                bottom_num += 1;
                bottom = format!("{:03b}", bottom_num).replace('0', ".").replace('1', "#");

            }
            middle_hash.insert(middle.clone(), bottom_hash );
            middle_num += 1;
            middle = format!("{:03b}", middle_num).replace('0', ".").replace('1', "#");

        }
        top_hash.insert(top.clone(), middle_hash );
        top_num += 1;
        top = format!("{:03b}", top_num).replace('0', ".").replace('1', "#");

    }

    let mut image = vec![];
    let lines = image_raw.split('\n').collect::<Vec<&str>>();
    let fillter = ".".repeat(lines[0].len() + (2*3));
    image.push(fillter.to_owned());
    image.push(fillter.to_owned());
    image.push(fillter.to_owned());
    for line in lines{
        image.push(format!("...{}...", line));
    }
    image.push(fillter.to_owned());
    image.push(fillter.to_owned());
    image.push(fillter.to_owned());

    (top_hash, image)
}




pub fn part1((algo, image): &(HashMap<String, HashMap<String, HashMap<String, char>>>, Vec<String>)) -> u64 {
    let first = image_process((algo, image));
    let second = image_process((algo, &first));
    count(&second)
}

fn image_process((algo, image): (&HashMap<String, HashMap<String, HashMap<String, char>>>, &Vec<String>)) -> Vec<String>{
    let (expanded, mut tmp) = image_expand(image);

    for row in 2..(expanded.len()-2){
        for col in 2..(expanded[row].len()-2){
            let top = expanded[row-1][(col-1)..=(col+1)].to_owned();
            let mid = expanded[row][(col-1)..=(col+1)].to_owned();
            let bottom = expanded[row+1][(col-1)..=(col+1)].to_owned();
            //println!("{}", top);

            if let Some(x) = algo.get(&top){
                if let Some(y) = x.get(&mid){
                    if let Some(z) = y.get(&bottom){
                        if z == &'.' {
                            tmp[row].replace_range(col..=col, ".");
                        } else {
                            tmp[row].replace_range(col..=col, "#");
                        }

                    }
                }
            }
        }
    }
    //println!("{:#?}",result);

    let mut result = vec![];


    for (i, line) in tmp.iter().enumerate() {
        if i < 2 {
            continue;
        }
        let line_new = line[2..line.len()-2].to_owned();
        result.push(line_new);
    }
    result.pop();
    result.pop();

    result
}

fn image_expand(image: &Vec<String>) -> (Vec<String>,Vec<String>) {
    let mut result:Vec<String> = vec![];
    let mut empty:Vec<String> = vec![];
    let sample = &image[0][..1];
    let top_bottom= sample.repeat(image[0].len()+(2*3));
    result.push(top_bottom.to_owned());
    result.push(top_bottom.to_owned());
    result.push(top_bottom.to_owned());

    for line in image {
        result.push(format!("{s}{s}{s}{}{s}{s}{s}", line, s=sample));
        empty.push(top_bottom.to_owned());
    }

    result.push(top_bottom.to_owned());
    result.push(top_bottom.to_owned());
    result.push(top_bottom.to_owned());

    empty.push(top_bottom.to_owned());
    empty.push(top_bottom.to_owned());
    empty.push(top_bottom.to_owned());
    empty.push(top_bottom.to_owned());
    empty.push(top_bottom.to_owned());
    empty.push(top_bottom.to_owned());

    (result, empty)
}

fn count(image: &Vec<String>) -> u64{
    let mut lit = 0;
    for line in image {
        for char in line.chars(){
            if char == '#'{
                lit += 1;
            }
        }
    }
    lit
}


pub fn part2((algo, image): &(HashMap<String, HashMap<String, HashMap<String, char>>>, Vec<String>)) -> u64 {
    let mut result = image.to_owned();
    for _ in 0..50 {
        result = image_process((algo, &result));
    }
    count(&result)
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE1: &'static str = "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###";

    const EXAMPLE2: &'static str = "#...............................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................

#";


    #[test]
    fn part1_test1() {
        assert_eq!(part1(&generator(EXAMPLE1)), 35);
    }

    #[test]
    fn part1_test2() {
        assert_eq!(part1(&generator(EXAMPLE2)), 1);
    }

    #[test]
    fn part2_test1() {
        assert_eq!(part2(&generator(EXAMPLE1)), 3351);
    }

    #[test]
    fn part2_test2() {
        assert_eq!(part2(&generator(EXAMPLE2)), 1);
    }

}
