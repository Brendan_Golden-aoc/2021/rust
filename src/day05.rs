#[derive(Debug)]
pub struct Generator {
    max: (usize, usize),
    lines: Vec<Line>,
}
#[derive(Debug)]
pub struct Line {
    start: (usize, usize),
    end: (usize, usize),
}

pub fn generator(input: &str) -> Generator {
    let mut lines: Vec<Line> = vec![];

    let mut max = (0, 0);

    for line in input.split('\n') {
        let coordinates = line.split(" -> ").collect::<Vec<_>>();

        let start = get_coordinates(coordinates[0]);
        let end = get_coordinates(coordinates[1]);

        // figure out teh bounds
        if start.0 > max.0 {
            max.0 = start.0;
        }
        if end.0 > max.0 {
            max.0 = end.0;
        }

        if start.1 > max.1 {
            max.1 = start.1;
        }
        if end.1 > max.1 {
            max.1 = end.1;
        }

        lines.push(Line { start, end })
    }

    Generator { max, lines }
}

fn get_coordinates(input: &str) -> (usize, usize) {
    let numbers_raw = input.split(',').collect::<Vec<_>>();
    (numbers_raw[0].parse::<usize>().unwrap_or(0), numbers_raw[1].parse::<usize>().unwrap_or(0))
}

pub fn part1(input: &Generator) -> i32 {
    general(input).0
}

pub fn part2(input: &Generator) -> i32 {
    general(input).1
}

fn general(input: &Generator) -> (i32, i32) {
    let mut grid_part1 = vec![vec![0; input.max.0 + 1]; input.max.1 + 1];
    let mut grid_part2 = vec![vec![0; input.max.0 + 1]; input.max.1 + 1];

    let mut part_1 = 0;
    let mut part_2 = 0;

    for line in &input.lines {
        if line.start.0 == line.end.0 {
            let (start, end) = if line.start.1 < line.end.1 { (line.start.1, line.end.1) } else { (line.end.1, line.start.1) };

            for i in start..=end {
                if grid_part1[i][line.start.0] == 1 {
                    part_1 += 1;
                }
                grid_part1[i][line.start.0] += 1;

                if grid_part2[i][line.start.0] == 1 {
                    part_2 += 1;
                }
                grid_part2[i][line.start.0] += 1;
            }
        } else if line.start.1 == line.end.1 {
            let (start, end) = if line.start.0 < line.end.0 { (line.start.0, line.end.0) } else { (line.end.0, line.start.0) };

            for i in start..=end {
                if grid_part1[line.start.1][i] == 1 {
                    part_1 += 1;
                }
                grid_part1[line.start.1][i] += 1;

                if grid_part2[line.start.1][i] == 1 {
                    part_2 += 1;
                }
                grid_part2[line.start.1][i] += 1;
            }
        } else {
            let mut i = line.start.1;
            let mut j = line.start.0;
            loop {
                if grid_part2[i][j] == 1 {
                    part_2 += 1;
                }
                grid_part2[i][j] += 1;

                if i == line.end.1 {
                    break;
                }
                if j == line.end.0 {
                    break;
                }

                if line.end.1 < line.start.1 {
                    i -= 1;
                } else {
                    i += 1;
                }

                if line.end.0 < line.start.0 {
                    j -= 1;
                } else {
                    j += 1;
                }
            }
        }
    }

    (part_1, part_2)
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2";

    #[test]
    fn part1_test() {
        assert_eq!(part1(&generator(EXAMPLE)), 5);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&generator(EXAMPLE)), 12);
    }
}
