pub fn generator(input: &str) -> Packet {
    let bin = generator_hex_to_bin(input);

    generator_bin_to_packets(&bin, 0).0
}

// may not be great doing it this way
fn generator_hex_to_bin(input: &str) -> String {
    let mut result = String::new();

    for character in input.chars() {
        // looked at documentation but didnt find better way
        let bin = match character {
            '0' => "0000",
            '1' => "0001",
            '2' => "0010",
            '3' => "0011",
            '4' => "0100",
            '5' => "0101",
            '6' => "0110",
            '7' => "0111",
            '8' => "1000",
            '9' => "1001",
            'A' => "1010",
            'B' => "1011",
            'C' => "1100",
            'D' => "1101",
            'E' => "1110",
            'F' => "1111",
            _ => "",
        };
        result.push_str(bin);
    }

    result
}

#[derive(Debug)]
pub struct Packet {
    version: u8,
    type_id: u8,
    value: Option<u64>,
    packets: Vec<Packet>,
}

fn generator_bin_to_packets(input: &str, depth: usize) -> (Packet, usize) {
    let mut packet = Packet { version: 0, type_id: 0, value: None, packets: vec![] };

    let version = input.get(0..3).unwrap();
    packet.version = u8::from_str_radix(version, 2).unwrap();

    let type_id = input.get(3..6).unwrap();
    packet.type_id = u8::from_str_radix(type_id, 2).unwrap();

    let mut position;

    //println!("{} {}   {}",depth, packet.type_id, input);
    if packet.type_id == 4 {
        // dealing with literal
        position = 6;
        let mut number = String::new();
        loop {
            let offset = if position + 5 > input.len() { (input.len() - 1) - position } else { 5 };

            let segment = &input[position..(position + offset)];
            number.push_str(&segment[1..]);
            if offset < 5 {
                number.push_str(&"0".repeat(5 - offset));
            }

            if &segment[..1] == "0" {
                position += offset;
                break;
            }

            // next round
            if position == input.len() {
                break;
            }
            position += 5;
            if position > input.len() {
                break;
            }
        }
        packet.value = Some(u64::from_str_radix(&number, 2).unwrap());
    } else {
        // only 4 is literal
        if &input[6..7] == "0" {
            //println!("here 0");
            let length = usize::from_str_radix(&input[7..(7 + 15)], 2).unwrap();

            // get first
            let mut start = 7 + 15;
            let mut remaining = &input[start..(22 + length)];

            while remaining.len() >= 11 {
                //println!("1   {} {} {} {}   {}",depth,  start, remaining.len(),length, remaining);

                let (sub_packet, sub_length) = generator_bin_to_packets(remaining, depth + 1);
                packet.packets.push(sub_packet);
                start += sub_length;
                //println!("2   {} {} {} {} sub_length: {}  {}",depth,  start, remaining.len(),length, sub_length, remaining);
                if remaining.len() == 11 {
                    break;
                }

                remaining = &input[start..(22 + length)];
            }

            position = 22 + length;
        } else {
            //println!("here 1");
            let sum_packets = usize::from_str_radix(&input[7..(7 + 11)], 2).unwrap();

            // get first
            let mut start = 7 + 11;
            let mut remaining = &input[start..];

            let mut completed = 0;

            while completed < sum_packets {
                let (sub_packet, sub_length) = generator_bin_to_packets(remaining, depth + 1);
                packet.packets.push(sub_packet);

                start += sub_length;
                remaining = &input[start..];

                completed += 1;
            }

            position = start;
        }
    }

    //println!("{:?}", packet);

    (packet, position)
}

pub fn part1(input: &Packet) -> u64 {
    let mut versions = 0;

    versions += input.version as u64;

    for sub_packet in &input.packets {
        versions += part1(sub_packet);
    }

    versions
}

pub fn part2(input: &Packet) -> u64 {
    let mut value = 0;

    let mut values = vec![];
    for sub_packet in &input.packets {
        values.push(part2(sub_packet));
    }

    // now do teh calcualtions
    match input.type_id {
        0 => {
            // sum

            for number in values {
                value += number;
            }
        }
        1 => {
            // product

            value = 1;
            for number in values {
                value *= number;
            }
        }
        2 => {
            // min

            value = u64::MAX;
            for number in values {
                if number < value {
                    value = number;
                }
            }
        }
        3 => {
            // max

            value = u64::MIN;
            for number in values {
                if number > value {
                    value = number;
                }
            }
        }
        4 => {
            // literal one

            if let Some(x) = input.value {
                value = x;
            }
        }
        5 => {
            // greater than

            if values.len() == 2 && values[0] > values[1] {
                value = 1;
            }
        }
        6 => {
            // less than

            if values.len() == 2 && values[0] < values[1] {
                value = 1;
            }
        }
        7 => {
            // equal

            if values.len() == 2 && values[0] == values[1] {
                value = 1;
            }
        }
        _ => {}
    }

    value
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE1: &'static str = "8A004A801A8002F478";
    const EXAMPLE2: &'static str = "620080001611562C8802118E34";
    const EXAMPLE3: &'static str = "C0015000016115A2E0802F182340";
    const EXAMPLE4: &'static str = "A0016C880162017C3686B18A3D4780";

    #[test]
    fn part1_test1() {
        assert_eq!(part1(&generator(EXAMPLE1)), 16);
    }
    #[test]
    fn part1_test2() {
        assert_eq!(part1(&generator(EXAMPLE2)), 12);
    }
    #[test]
    fn part1_test3() {
        assert_eq!(part1(&generator(EXAMPLE3)), 23);
    }
    #[test]
    fn part1_test4() {
        assert_eq!(part1(&generator(EXAMPLE4)), 31);
    }

    const EXAMPLE5: &'static str = "C200B40A82";
    const EXAMPLE6: &'static str = "04005AC33890";
    const EXAMPLE7: &'static str = "880086C3E88112";
    const EXAMPLE8: &'static str = "CE00C43D881120";
    const EXAMPLE9: &'static str = "D8005AC2A8F0";
    const EXAMPLE10: &'static str = "F600BC2D8F";
    const EXAMPLE11: &'static str = "9C005AC2F8F0";
    const EXAMPLE12: &'static str = "9C0141080250320F1802104A08";

    #[test]
    fn part2_test1() {
        assert_eq!(part2(&generator(EXAMPLE5)), 3);
    }

    #[test]
    fn part2_test2() {
        assert_eq!(part2(&generator(EXAMPLE6)), 54);
    }
    #[test]
    fn part2_test3() {
        assert_eq!(part2(&generator(EXAMPLE7)), 7);
    }

    #[test]
    fn part2_test4() {
        assert_eq!(part2(&generator(EXAMPLE8)), 9);
    }
    #[test]
    fn part2_test5() {
        assert_eq!(part2(&generator(EXAMPLE9)), 1);
    }
    #[test]
    fn part2_test6() {
        assert_eq!(part2(&generator(EXAMPLE10)), 0);
    }
    #[test]
    fn part2_test7() {
        assert_eq!(part2(&generator(EXAMPLE11)), 0);
    }
    #[test]
    fn part2_test8() {
        assert_eq!(part2(&generator(EXAMPLE12)), 1);
    }
}
