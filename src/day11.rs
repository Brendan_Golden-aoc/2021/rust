pub fn generator(input: &str) -> Vec<Vec<u8>> {
    let mut result: Vec<Vec<u8>> = Vec::new();

    for line in input.split('\n') {
        let mut result_inner: Vec<u8> = Vec::new();

        for bit in line.split("") {
            if bit.is_empty() {
                continue;
            }
            result_inner.push(bit.parse::<u8>().unwrap())
        }
        result.push(result_inner)
    }

    result
}

pub fn part1(input: &[Vec<u8>]) -> u32 {
    let mut octopi = input.to_owned();

    let mut rounds = 0;
    let mut flash = 0;

    while rounds < 100 {
        // incriment all numbers by 1
        for row in &mut octopi {
            for octopus in row {
                *octopus += 1;
            }
        }

        // test new config until stable
        let (octopi_tmp, flash_sub) = process_lights(octopi);
        octopi = octopi_tmp;
        flash += flash_sub;

        rounds += 1;
    }

    /*
    for octopus in octopi {
        println!("{:?}", octopus)
    }
     */

    flash
}

fn process_lights(octopi: Vec<Vec<u8>>) -> (Vec<Vec<u8>>, u32) {
    let mut octopi_next = octopi.to_owned();
    let mut flash = 0;

    for (i, row) in octopi.iter().enumerate() {
        for (j, octopus) in row.iter().enumerate() {
            if octopus > &9 {
                // it flashed
                flash += 1;
                octopi_next[i][j] = 0;

                for k in ((i as isize) - 1)..=((i as isize) + 1) {
                    if k < 0 {
                        continue;
                    }
                    let k = k as usize;
                    if k >= octopi.len() {
                        continue;
                    }

                    for l in ((j as isize) - 1)..=((j as isize) + 1) {
                        if l < 0 {
                            continue;
                        }
                        let l = l as usize;
                        if l >= octopi[i].len() {
                            continue;
                        }

                        if octopi_next[k][l] > 0 {
                            octopi_next[k][l] += 1;
                        }
                    }
                }
            }
        }
    }

    if flash > 0 {
        let (octopi_tmp, flash_sub) = process_lights(octopi_next);
        octopi_next = octopi_tmp;
        flash += flash_sub;
    }

    (octopi_next, flash)
}

pub fn part2(input: &[Vec<u8>]) -> u32 {
    let mut octopi = input.to_owned();

    let mut rounds = 0;

    loop {
        // incriment all numbers by 1
        let mut all_0s = true;
        for row in &mut octopi {
            for octopus in row {
                if all_0s && octopus > &mut 0 {
                    all_0s = false;
                }
                *octopus += 1;
            }
        }

        if all_0s {
            break;
        }

        // test new config until stable
        let (octopi_tmp, _) = process_lights(octopi);
        octopi = octopi_tmp;

        rounds += 1;
    }

    /*
    for octopus in octopi {
        println!("{:?}", octopus)
    }
     */

    rounds
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526";

    #[test]
    fn part1_test() {
        assert_eq!(part1(&generator(EXAMPLE)), 1656);
    }

    ///*
    #[test]
    fn part2_test() {
        assert_eq!(part2(&generator(EXAMPLE)), 195);
    }

    // */
}
