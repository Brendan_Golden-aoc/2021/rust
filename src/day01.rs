pub fn generator(input: &str) -> Vec<i32> {
    let mut result: Vec<i32> = Vec::new();

    for s in input.split('\n') {
        result.push(s.parse::<i32>().unwrap())
    }

    result
}

pub fn part1(input: &[i32]) -> i32 {
    let mut increased = 0;
    let mut last = &i32::MAX;

    for number in input {
        if number > last {
            increased += 1;
        }
        last = number;
    }

    increased
}

pub fn part2(input: &[i32]) -> i32 {
    let mut increased = 0;
    let mut last = i32::MAX;

    // starting on the 3rd one
    let mut i = 2;
    loop {
        if i >= input.len() {
            break;
        }
        let sum = input[i] + input[i - 1] + input[i - 2];

        if sum > last {
            increased += 1;
        }
        last = sum;

        i += 1;
    }

    increased
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "199
200
208
210
200
207
240
269
260
263";

    #[test]
    fn part1_test() {
        assert_eq!(part1(&generator(EXAMPLE)), 7);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&generator(EXAMPLE)), 5);
    }
}
