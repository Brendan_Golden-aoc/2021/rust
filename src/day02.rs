pub struct Command<'a> {
    action: &'a str,
    distance: i32,
}

pub fn generator(input: &str) -> Vec<Command> {
    let mut result: Vec<Command> = Vec::new();

    for line in input.split('\n') {
        let line_split: Vec<&str> = line.split(' ').collect();
        let command = Command { action: line_split[0], distance: line_split[1].parse::<i32>().unwrap() };

        result.push(command)
    }

    result
}

pub fn part1(input: &[Command]) -> i32 {
    let mut horizontal = 0;
    let mut depth = 0;

    for command in input {
        match command.action {
            "forward" => {
                horizontal += command.distance;
            }
            "down" => {
                depth += command.distance;
            }
            "up" => {
                depth -= command.distance;
            }
            &_ => {}
        }
    }

    horizontal * depth
}

pub fn part2(input: &[Command]) -> i32 {
    let mut horizontal = 0;
    let mut depth = 0;
    let mut aim = 0;

    for command in input {
        match command.action {
            "forward" => {
                horizontal += command.distance;
                depth += command.distance * aim;
            }
            "down" => {
                aim += command.distance;
            }
            "up" => {
                aim -= command.distance;
            }
            &_ => {}
        }
    }

    horizontal * depth
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "forward 5
down 5
forward 8
up 3
down 8
forward 2";

    #[test]
    fn part1_test() {
        assert_eq!(part1(&generator(EXAMPLE)), 150);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&generator(EXAMPLE)), 900);
    }
}
