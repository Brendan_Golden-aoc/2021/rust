pub fn generator(input: &str) -> Vec<i32> {
    let mut fishes: Vec<i32> = vec![];

    for line in input.split('\n') {
        for number in line.split(',') {
            if let Ok(x) = number.parse::<i32>() {
                fishes.push(x)
            }
        }
    }

    fishes
}

pub fn part1(input: &[i32]) -> usize {
    general(input, 80)
}

pub fn part2(input: &[i32]) -> usize {
    general(input, 256)
}

fn general(input: &[i32], days: i32) -> usize {
    // dont need to model every fish, just mneed to model each fish as a groupo
    let mut buckets: Vec<usize> = vec![0; 11];

    // split the fish into buckets
    for fish in input {
        buckets[*fish as usize] += 1;
    }

    // 80 turns
    for _n in 1..=days {
        for i in 0..=10 {
            if i == 0 {
                buckets[7] += buckets[0];
                buckets[9] = buckets[0];
            } else {
                buckets[i - 1] = buckets[i]
            }
        }
    }

    buckets.iter().sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "3,4,3,1,2";

    #[test]
    fn part1_test() {
        assert_eq!(part1(&generator(EXAMPLE)), 5934);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&generator(EXAMPLE)), 26984457539);
    }
}
