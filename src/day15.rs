pub fn generator(input: &str) -> Vec<Vec<u64>> {
    let mut result: Vec<Vec<u64>> = Vec::new();

    for line in input.split('\n') {
        let mut result_inner: Vec<u64> = Vec::new();

        for number in line.split("") {
            if number.is_empty() {
                continue;
            }
            result_inner.push(number.parse::<u64>().unwrap())
        }
        result.push(result_inner)
    }

    result
}

pub fn part1(input: &[Vec<u64>]) -> u64 {
    maze_runner(input)
}

fn maze_runner(input: &[Vec<u64>]) -> u64 {
    // create an array of
    let mut map = vec![vec![u64::MAX; input[0].len()]; input.len()];

    // set first position to be 0
    map[0][0] = 0;

    let mut to_visit = vec![(0, 0)];

    while !to_visit.is_empty() {
        // go to first entry
        let (i, j) = to_visit[0];

        // check if it already visited

        // update neighbours
        let cell_distance = map[i][j];

        // add neighbours to the to visit if the value updates

        // up?
        if i > 0 {
            // distance of teh cell from teh neighbours is the number in teh cell
            let new_distance = input[i - 1][j] + cell_distance;

            if new_distance < map[i - 1][j] {
                map[i - 1][j] = new_distance;
                to_visit.push((i - 1, j))
            }
        }

        // down
        if (i + 1) < input.len() {
            // distance of teh cell from teh neighbours is the number in teh cell
            let new_distance = input[i + 1][j] + cell_distance;

            if new_distance < map[i + 1][j] {
                map[i + 1][j] = new_distance;
                to_visit.push((i + 1, j))
            }
        }

        // right
        if (j + 1) < input[i].len() {
            // distance of teh cell from teh neighbours is the number in teh cell
            let new_distance = input[i][j + 1] + cell_distance;

            if new_distance < map[i][j + 1] {
                map[i][j + 1] = new_distance;
                to_visit.push((i, j + 1))
            }
        }

        // left?
        if j > 0 {
            // distance of teh cell from teh neighbours is the number in teh cell
            let new_distance = input[i][j - 1] + cell_distance;

            if new_distance < map[i][j - 1] {
                map[i][j - 1] = new_distance;
                to_visit.push((i, j - 1))
            }
        }

        // remove first entry
        to_visit.remove(0);
    }

    map[input.len() - 1][input[0].len() - 1]
}

pub fn part2(input: &[Vec<u64>]) -> u64 {
    // take the input, generate a maze 25 times bigger, then solve
    let input_new = maze_builder(input);

    maze_runner(&input_new)
}

fn maze_builder(input: &[Vec<u64>]) -> Vec<Vec<u64>> {
    let mut result = vec![vec![0; input[0].len() * 5]; input.len() * 5];

    for (i, row) in input.iter().enumerate() {
        for (j, cell) in row.iter().enumerate() {
            result[i][j] = *cell;

            for j_offset in 1..5 {
                let j_new = j + (row.len() * j_offset);
                let j_last = j + (row.len() * (j_offset - 1));

                let mut new_value = result[i][j_last] + 1;
                if new_value > 9 {
                    new_value = 1;
                }

                result[i][j_new] = new_value;
            }
        }
    }

    for i in 0..input.len() {
        for j in 0..result[i].len() {
            for i_offset in 1..5 {
                let i_new = i + (input[i].len() * i_offset);
                let i_last = i + (input[i].len() * (i_offset - 1));

                let mut new_value = result[i_last][j] + 1;
                if new_value > 9 {
                    new_value = 1;
                }

                result[i_new][j] = new_value;
            }
        }
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581";

    const EXAMPLE2: &'static str = "9111
9991
1111
1999
1111";

    #[test]
    fn part1_test() {
        assert_eq!(part1(&generator(EXAMPLE)), 40);
    }

    #[test]
    fn part1_test2() {
        assert_eq!(part1(&generator(EXAMPLE2)), 13);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&generator(EXAMPLE)), 315);
    }
}
