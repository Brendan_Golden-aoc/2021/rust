pub fn generator(input: &str) -> Vec<Vec<u8>> {
    let mut result: Vec<Vec<u8>> = Vec::new();

    for line in input.split('\n') {
        let mut result_inner: Vec<u8> = Vec::new();

        for bit in line.split("") {
            if bit.is_empty() {
                continue;
            }
            result_inner.push(bit.parse::<u8>().unwrap())
        }
        result.push(result_inner)
    }

    result
}

pub fn part1(input: &[Vec<u8>]) -> i32 {
    let mut counter = vec![0; input[0].len()];
    for line in input {
        for (i, bit) in line.iter().enumerate() {
            if bit == &1 {
                counter[i] += 1;
            }
        }
    }

    let mut gamma_bin: String = "".to_owned();
    let mut epsilon_bin: String = "".to_owned();

    for bit in counter {
        if (bit as usize) < (input.len() / 2) {
            gamma_bin.push('0');
            epsilon_bin.push('1');
        } else {
            gamma_bin.push('1');
            epsilon_bin.push('0');
        }
    }

    let gamma = i32::from_str_radix(&gamma_bin, 2).unwrap();
    let epsilon = i32::from_str_radix(&epsilon_bin, 2).unwrap();

    gamma * epsilon
}

pub fn part2(input: &[Vec<u8>]) -> i32 {
    get_general(input, "oxygen", 0) * get_general(input, "co2", 0)
}

fn get_general(input: &[Vec<u8>], monitor: &str, search_bit: usize) -> i32 {
    // find the most common bit
    let mut ones = 0;
    let mut zeros = 0;
    for line in input {
        if line[search_bit] == 1 {
            ones += 1;
        } else {
            zeros += 1;
        }
    }

    // oxygen
    let main_bit = if zeros > ones {
        if monitor == "oxygen" {
            0
        } else {
            1
        }
    } else if monitor == "oxygen" {
        1
    } else {
        0
    };

    let mut result: Vec<Vec<u8>> = Vec::new();

    for line in input {
        if line[search_bit] == main_bit {
            result.push(line.to_owned())
        }
    }

    if result.len() == 1 {
        vec_bin_to_i32(&result[0])
    } else {
        get_general(&result, monitor, search_bit + 1)
    }
}

fn vec_bin_to_i32(input: &[u8]) -> i32 {
    let joined: String = input.iter().map(|&id| id.to_string()).collect();
    i32::from_str_radix(&joined, 2).unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010";

    #[test]
    fn part1_test() {
        assert_eq!(part1(&generator(EXAMPLE)), 198);
    }

    ///*
    #[test]
    fn part2_test() {
        assert_eq!(part2(&generator(EXAMPLE)), 230);
    }

    // */
}
