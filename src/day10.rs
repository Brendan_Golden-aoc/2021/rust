pub fn generator(input: &str) -> Vec<Vec<char>> {
    let mut result: Vec<Vec<char>> = Vec::new();

    for line in input.split('\n') {
        result.push(line.chars().collect::<Vec<_>>());
    }

    result
}

pub fn part1(input: &[Vec<char>]) -> u32 {
    // ( [ { <

    let mut score = 0;
    for line in input {
        let mut openers = vec![];

        for character in line {
            match character {
                '(' | '[' | '{' | '<' => {
                    // openers
                    openers.push(character)
                }
                ')' | ']' | '}' | '>' => {
                    // closers

                    let (opener, char_score) = match character {
                        ')' => ('(', 3),
                        ']' => ('[', 57),
                        '}' => ('{', 1197),
                        '>' => ('<', 25137),
                        _ => (' ', 0),
                    };

                    if openers[openers.len() - 1] == &opener {
                        // if you have found teh closer then remove it from teh list of openers
                        openers.remove(openers.len() - 1);
                    } else {
                        score += char_score;
                        break;
                    }
                }
                _ => {}
            }
        }
    }

    score
}

pub fn part2(input: &[Vec<char>]) -> u64 {
    let mut scores = vec![];
    'lines: for line in input {
        let mut openers = vec![];

        for character in line {
            match character {
                '(' | '[' | '{' | '<' => {
                    // openers
                    openers.push(character)
                }
                ')' | ']' | '}' | '>' => {
                    // closers

                    let (opener, _char_score) = match character {
                        ')' => ('(', 3),
                        ']' => ('[', 57),
                        '}' => ('{', 1197),
                        '>' => ('<', 25137),
                        _ => (' ', 0),
                    };

                    if openers[openers.len() - 1] == &opener {
                        // if you have found teh closer then remove it from teh list of openers
                        openers.remove(openers.len() - 1);
                    } else {
                        continue 'lines;
                    }
                }
                _ => {}
            }
        }

        let mut score_line = 0;
        openers.reverse();

        for character in openers {
            score_line *= 5;
            match character {
                '(' => {
                    score_line += 1;
                }
                '[' => {
                    score_line += 2;
                }
                '{' => {
                    score_line += 3;
                }
                '<' => {
                    score_line += 4;
                }
                _ => {}
            }
        }
        scores.push(score_line);
    }

    scores.sort_unstable();

    // get teh middle
    scores[scores.len() / 2]
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]";

    #[test]
    fn part1_test() {
        assert_eq!(part1(&generator(EXAMPLE)), 26397);
    }

    ///*
    #[test]
    fn part2_test() {
        assert_eq!(part2(&generator(EXAMPLE)), 288957);
    }

    // */
}
