pub fn generator(input: &str) -> Vec<Vec<u8>> {
    let mut result: Vec<Vec<u8>> = Vec::new();

    for line in input.split('\n') {
        let mut result_inner: Vec<u8> = Vec::new();

        for bit in line.split("") {
            if bit.is_empty() {
                continue;
            }
            result_inner.push(bit.parse::<u8>().unwrap())
        }
        result.push(result_inner)
    }

    result
}

pub fn part1(input: &[Vec<u8>]) -> u32 {
    let mut risk = 0;

    for (i, row) in input.iter().enumerate() {
        for (j, col) in row.iter().enumerate() {
            // i + 1, j
            if (i + 1) < input.len() && &input[i + 1][j] <= col {
                continue;
            }

            // i - 1, j
            if ((i as isize) - 1) >= 0 && &input[i - 1][j] <= col {
                continue;
            }

            // i, j + 1
            if (j + 1) < row.len() && &input[i][j + 1] <= col {
                continue;
            }

            // i, j - 1
            if ((j as isize) - 1) >= 0 && &input[i][j - 1] <= col {
                continue;
            }

            risk += 1 + (*col as u32);
        }
    }

    risk
}

#[derive(Debug)]
struct Basin {
    // center: (usize, usize),
    // number: u32,
    size: u32,
}

pub fn part2(input: &[Vec<u8>]) -> u32 {
    let mut map = vec![vec![0; input[0].len()]; input.len()];

    let mut basins = 0;

    let mut basin_vec: Vec<Basin> = vec![];

    for (i, row) in input.iter().enumerate() {
        for (j, col) in row.iter().enumerate() {
            // i - 1, j
            if (i + 1) < input.len() && &input[i + 1][j] <= col {
                continue;
            }

            // i - 1, j
            if ((i as isize) - 1) >= 0 && &input[i - 1][j] <= col {
                continue;
            }

            // i, j + 1
            if (j + 1) < row.len() && &input[i][j + 1] <= col {
                continue;
            }

            // i, j - 1
            if ((j as isize) - 1) >= 0 && &input[i][j - 1] <= col {
                continue;
            }

            // lowest point
            basins += 1;
            // search around this part

            // set teh center
            map[i][j] = basins;

            let (map_tmp, count_tmp) = get_basin(input, map, 1, basins, i, j);
            map = map_tmp;

            basin_vec.push(Basin {
                //center: (i, j),
                //number: basins,
                size: count_tmp,
            });
        }
    }

    /*
    for line in map {
        println!("{:?}", line);
    }
     */

    basin_vec.sort_by(|a, b| b.size.cmp(&a.size));

    let mut largest = 1;
    for (i, basin) in basin_vec.iter().enumerate() {
        if i > 2 {
            break;
        }
        largest *= basin.size;
    }

    largest
}

fn get_basin(input: &[Vec<u8>], mut map: Vec<Vec<u32>>, mut count: u32, basins: u32, i: usize, j: usize) -> (Vec<Vec<u32>>, u32) {
    let current_cell = input[i][j];

    // i + 1, j
    if (i + 1) < input.len() && input[i + 1][j] > current_cell && map[i + 1][j] == 0 && input[i + 1][j] < 9 {
        map[i + 1][j] = basins;
        count += 1;
        // test it out and return
        let (map_tmp, count_tmp) = get_basin(input, map, count, basins, i + 1, j);
        map = map_tmp;
        count = count_tmp;
    }

    // i - 1, j
    if ((i as isize) - 1) >= 0 && input[i - 1][j] > current_cell && map[i - 1][j] == 0 && input[i - 1][j] < 9 {
        map[i - 1][j] = basins;
        count += 1;
        // test it out and return
        let (map_tmp, count_tmp) = get_basin(input, map, count, basins, i - 1, j);
        map = map_tmp;
        count = count_tmp;
    }

    // i, j + 1
    if (j + 1) < input[i].len() && input[i][j + 1] > current_cell && map[i][j + 1] == 0 && input[i][j + 1] < 9 {
        map[i][j + 1] = basins;
        count += 1;
        // test it out and return
        let (map_tmp, count_tmp) = get_basin(input, map, count, basins, i, j + 1);
        map = map_tmp;
        count = count_tmp;
    }

    if ((j as isize) - 1) >= 0 {
        // left
        //println!("{} {} current: {} left: {} map:{}", i, j, &current_cell, &input[i][j-1], &map[i][j-1]);
        if input[i][j - 1] > current_cell && map[i][j - 1] == 0 && input[i][j - 1] < 9 {
            map[i][j - 1] = basins;
            count += 1;
            // test it out and return
            let (map_tmp, count_tmp) = get_basin(input, map, count, basins, i, j - 1);
            map = map_tmp;
            count = count_tmp;
        }
    }
    (map, count)
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &'static str = "2199943210
3987894921
9856789892
8767896789
9899965678";

    #[test]
    fn part1_test() {
        assert_eq!(part1(&generator(EXAMPLE)), 15);
    }

    ///*
    #[test]
    fn part2_test() {
        assert_eq!(part2(&generator(EXAMPLE)), 1134);
    }

    // */
}
